import {createRouter, createWebHashHistory, RouteRecordRaw} from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'homepage',
        component: () => import('../pages/homePage.vue')
    },
    {
        path: '/listener',
        name: 'listener',
        component: () => import('../pages/listener.vue')
    },
    {
        path: '/speaker',
        name: 'speaker',
        component: () => import('../pages/speaker.vue')
    }
    ];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;

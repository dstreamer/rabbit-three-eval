import { createRouter, createWebHashHistory } from "vue-router";
const routes = [
    {
        path: '/',
        name: 'homepage',
        component: () => import('../pages/index.vue')
    },
    {
        path: '/test',
        name: 'Home',
        component: () => import('../pages/home.vue')
    },
    {
        path: '/listener',
        name: 'listener',
        component: () => import('../pages/listener.vue')
    },
    {
        path: '/speaker',
        name: 'speaker',
        component: () => import('../pages/speaker.vue')
    }
];
const router = createRouter({
    history: createWebHashHistory(),
    routes,
});
export default router;
//# sourceMappingURL=index.js.map
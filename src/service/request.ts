/**
 * Created by lhs7248 on 2018/4/26.
 */
import axios from 'axios'
import router from '../router'

axios.defaults.timeout = 20000
const domainurl = 'http://121.199.9.127'
export const ExternalDomainurl = 'http://121.199.9.127'// 外部文件调用，需跟上面的地址保持一致

axios.interceptors.request.use(config => {
    return config
}, err => {
    //Toast('请求超时!')
    return Promise.resolve(err)
})
axios.interceptors.response.use(data => {
    return data
}, err => {
    if (err.response) {
    }
    return Promise.resolve(err)
})
export const postRequest = (url, params) => {

    return axios({
        method: 'post',
        url: `${url}`,
        data: params,
        withCredentials: true,
        transformRequest: [function (data) {
            let ret = ''
            for (let it in data) {
                ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            return ret
        }],
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; application/json; text/html'
        }
    })
}

export const postRequestFile = (url, params) => {

    return axios({
        method: 'post',
        url: `${url}`,
        data: params,
        responseType: 'blob',
        withCredentials: true,
        transformRequest: [function (data) {
            let ret = ''
            for (let it in data) {
                ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            return ret
        }],
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; application/json; text/html'
        }
    })
}

export const postJsonRequest = (url, params) => {

    axios.defaults.baseURL = domainurl

    return axios({
        method: 'post',
        url: `${url}`,
        data: params,
        withCredentials: true,
        headers: {
            'Content-Type': 'application/json; text/html'
        }
    })
}
export const uploadFileRequest = (url, params) => {
    console.info(url)
    return axios({
        method: 'post',
        url: `${url}`,
        data: params,

        withCredentials: true,
        headers: {}
    })
}
export const putRequest = (url, params) => {
    return axios({
        method: 'put',
        url: `${url}`,
        data: params,
        transformRequest: [function (data) {
            let ret = ''
            for (let it in data) {
                ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            return ret
        }],
        withCredentials: true,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}
export const deleteRequest = (url) => {
    return axios({
        method: 'delete',
        url: `${url}`
    })
}
export const getRequest = (url, params) => {

    axios.defaults.baseURL = domainurl
    return axios({
        method: 'get',
        params: params,
        url: `${url}`,
        withCredentials: false,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; application/json; text/html'
        }
    })
}
